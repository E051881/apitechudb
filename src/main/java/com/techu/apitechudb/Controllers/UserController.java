package com.techu.apitechudb.Controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<Object> getUserByAge(@RequestParam(value = "age", required = false ,defaultValue = "0")  int age) {
        System.out.println("getUserByAge");
        System.out.println("La edad del usuario a buscar es " + age);

        return new ResponseEntity<>(
                this.userService.findAll(age),
                HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("La descripcion del usuario a crear es " + user.getName());
        System.out.println("El precio del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("El Id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }


    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario que se va a actualizar en paránmetro es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId());
        System.out.println("La descripcion del usuario que se va a actualizar es " + user.getName());
        System.out.println("El precio del usuario que se va a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("la id del usuario a borrar" + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }
}
