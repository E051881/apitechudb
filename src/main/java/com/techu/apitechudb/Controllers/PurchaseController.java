package com.techu.apitechudb.Controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.purchaseService.finAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchasetById(@PathVariable String id) {
        System.out.println("getPurchaseByid");
        System.out.println("La id del purchase a buscar es " + id);

        Optional<PurchaseModel> result = this.purchaseService.getdById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Purchase no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id de la compra a crear es " + purchase.getId());
        System.out.println("El id del usuario de la compra a crear es " + purchase.getUserId());
        System.out.println("El precio de la compra a crear es " + purchase.getAmount());
        System.out.println("Productos/cantidad de la compra a crear es " + purchase.getPurchaseItems());

        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.add(purchase);

        return new ResponseEntity<>(
                this.purchaseService.add(purchase).getPurchase(),
                this.purchaseService.add(purchase).getResponseHttpStatusCode()
        );
    }
}
