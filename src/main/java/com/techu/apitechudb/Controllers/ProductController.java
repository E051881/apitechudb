package com.techu.apitechudb.Controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
             this.productService.finAll(),
             HttpStatus.OK
         );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto a crea es " + product.getId());
        System.out.println("La descripcion del producto a crea es " + product.getDesc());
        System.out.println("El precio del producto a crea es " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductByid");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Product no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto que se va a actualizar en paránmetro es " + id);
        System.out.println("La id del producto que se va a actualizar es " + product.getId());
        System.out.println("La descripcion del producto que se va a actualizar es " + product.getDesc());
        System.out.println("El precio del producto que se va a actualizar es " + product.getPrice());

        return new ResponseEntity<>(this.productService.update(product), HttpStatus.OK);

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("la id del producto a borrar" + id);

        boolean deleteProduct = this.productService.delete(id);


        return new ResponseEntity<>(
                deleteProduct ? "producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }
}
