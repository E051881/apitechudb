package com.techu.apitechudb.models;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class PurchaseModel {

//    @Autowired
//    ProductModel ProductModel;

    private String id;
    private String userId;
    private float amount;
    private Map<String, Integer> purchaseItems;

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, float amount, Map<String, Integer> purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public float getAmount() {
        return amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
