package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseRepository");


        return ApitechudbApplication.purchaseModels;

    }

    public PurchaseModel save(PurchaseModel purchasemodel) {

        System.out.println("Save en PurchaseModelRepository");

        ApitechudbApplication.purchaseModels.add(purchasemodel);

        return purchasemodel;

    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("FindById en PurchaseRepository");

        Optional<PurchaseModel> result = Optional.empty();

        for(PurchaseModel purchaseInList :  ApitechudbApplication.purchaseModels) {
            if (purchaseInList.getId().equals(id)) {
                System.out.println("Purchase encontrado");
                result = Optional.of(purchaseInList);
            }
        }

        return result;
    }
}
