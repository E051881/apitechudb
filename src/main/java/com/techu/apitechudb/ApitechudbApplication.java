package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<UserModel> userModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels= ApitechudbApplication.getTestDate();
		ApitechudbApplication.userModels= ApitechudbApplication.getTestDateUser();
		ApitechudbApplication.purchaseModels= ApitechudbApplication.getTestDatePurchaseModel();
	}

	private static ArrayList<ProductModel> getTestDate() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,10
				)
		);

		return productModels;
	}

	private static ArrayList<UserModel> getTestDateUser() {

		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1"
						,"Nombre 1"
						,11
				)
		);

		userModels.add(
				new UserModel(
						"2"
						,"Nombre 2"
						,11
				)
		);

		userModels.add(
				new UserModel(
						"3"
						,"Nombre 3"
						,13
				)
		);

		return userModels;
	}

	private static ArrayList<PurchaseModel> getTestDatePurchaseModel() {

		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();

		return purchaseModels;
	}

}
