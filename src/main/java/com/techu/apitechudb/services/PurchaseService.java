package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    ProductService productService;
    @Autowired
    UserService userService;

    public List<PurchaseModel> finAll() {

        System.out.println("findAll en PurchaseService");

        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel>getdById(String id) {
        System.out.println("findByID en PurchaseService ");

        return this.purchaseRepository.findById(id);
    }


    public PurchaseServiceResponse add(PurchaseModel purchase) {
        System.out.println("add en UserService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (this.userService.findById(purchase.getUserId()).isPresent() == false) {
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode((HttpStatus.BAD_REQUEST));

            return result;
        }

        if (this.getdById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (this.productService.findById(purchaseItem.getKey()).isPresent() == false) {
                System.out.println("El producto con la id" + purchaseItem.getKey() +
                        "no se encuentra en el sistema");
                result.setMsg("El producto con la id" + purchaseItem.getKey() +
                    "no se encuentra en el sistema");
                 result.setResponseHttpStatusCode((HttpStatus.BAD_REQUEST));

                 return result;
            } else {

            System.out.println("Añadiendo valor de " + purchaseItem.getValue() + " unidades del producto al total");

            amount += (this.productService.findById(purchaseItem.getKey()).get().getPrice()
                       * purchaseItem.getValue());
           }
        }



        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
        //return this.purchaseRepository.save(purchase);

    }
}
